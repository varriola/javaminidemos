/*
 * Código utilizado para el curso de Introducción a las Ciencias de la
 * Computación.
 * Se permite consultarlo para fines didácticos en forma personal.
 */
package minidemos.entradasalida;

import java.io.FileNotFoundException;
import java.io.PrintStream;

/**
 * Demo básico sobre el uso de PrintStream.
 * @author blackzafiro
 */
public class DemoPrintStream {
	/**
	 * Escribe el texto indicado en un archivo.
	 * @param args 
	 */
	public static void main(String[] args) {
		String nombreArchivo = "Salida.txt";
		try (PrintStream fout = new PrintStream(nombreArchivo)) {
			fout.println("Inicio");
			fout.format("Línea %d\n", 1);
			fout.println("Fin");
		} catch (FileNotFoundException fnfe) {
			System.err.println("No se encotró el archivo " + nombreArchivo + " y no pudo ser creado");
		} catch (SecurityException se) {
			System.err.println("No se tiene permiso de escribir en el archivo");
		}
	}
	
}
