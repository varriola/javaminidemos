/*
 * Código utilizado para el curso de Introducción a las Ciencias de la
 * Computación.
 * Se permite consultarlo para fines didácticos en forma personal.
 */
package minidemos.entradasalida;

import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Ensamblado a partir de:
 * https://docs.oracle.com/javase/tutorial/essential/io/dirs.html
 * @author blackzafiro
 */
public class Directorios {
	
	/**
	 * Filtro para identificar directorios.
	 */
	static DirectoryStream.Filter<Path> filter =
		new DirectoryStream.Filter<Path>() {
			public boolean accept(Path file) throws IOException {
				return (Files.isDirectory(file));
			}
		};
	
	/**
	 * Lista directorios recursivamente
	 * @param dir ruta al directorio a partir del cual se hará el listado.
	 */
	public static void listaDirectorios(Path dir) {
		System.out.println(dir);
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir, filter)) {
			for (Path file: stream) {
				listaDirectorios(file);
			}
		} catch (IOException | DirectoryIteratorException x) {
			// IOException can never be thrown by the iteration.
			// In this snippet, it can only be thrown by newDirectoryStream.
			System.err.println(x);
		}
	}
	
	/**
	 * Lista directorios a partir de la raíz o el directorio indicado.
	 * @param args 
	 */
	public static void main(String[] args) {
		Iterable<Path> dirs;
		if (args.length > 0) {
			dirs = Paths.get(args[0]);
		} else {
			dirs = FileSystems.getDefault().getRootDirectories();
		}
		
		for (Path dir: dirs) {
			listaDirectorios(dir);
		}
	}	
}
