/*
 * Código utilizado para el curso de Introducción a las Ciencias de la
 * Computación.
 * Se permite consultarlo para fines didácticos en forma personal.
 */
package minidemos.entradasalida;

import java.util.Scanner;

/**
 * Demo básico sobre el uso de la clase Scanner.
 * @author blackzafiro
 */
public class DemoScanner {
	
	/**
	 * Recibe una línea de texto desde el teclado y
	 * reimprime el mensaje que recibió, hasta que el usuario
	 * se despida.
	 * @param args 
	 */
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		while(s.hasNext()) {
			String linea = s.nextLine();
			//String linea = s.next();
			if (linea.equals("")) {
				// Sólo funciona después de ingresar una línea distinta de ""
				System.out.println("No me des el avión, escribe algo.");
			} else {
				System.out.println("Eco: " + linea);
			}
			if(linea.equals("Adios")) break;
		}
		s.close();
	}
	
}
