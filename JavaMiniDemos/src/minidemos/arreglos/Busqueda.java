/**
 * 
 */
package minidemos.arreglos;

import java.util.Arrays;

/**
 * Ejemplos de búsquedas en arreglos.
 * @author blackzafiro
 *
 */
public class Busqueda {
	
	/**
	 * Devuelve el índice en que se encuentra <code>num</code>,
	 * o -1 si no está, suponiendo que los elementos en <code>a</code>
	 * están ordenados de menor a mayor.
	 * @param a El arreglo ordenado.
	 * @param num El elemento que se busca
	 * @return El índice donde se encuentra, -1 si no está.
	 */
	public static int binaria(int[] a, int num) {
		if (a == null) throw new NullPointerException();
		if (a.length == 0 || num < a[0] || num > a[a.length -1]) return -1;
		int ini = 0;
		int fin = a.length - 1;
		int m = fin / 2;
		while (a[m] != num) {
			if(a[m] > num) {
				// Izquierda
				fin = m - 1;
			} else {
				// Derecha
				ini = m + 1;
			}
			if (fin < ini) return -1;
			m = (fin + ini) / 2;
		}
		return m;
	}

	/**
	 * Prueba si el método <code>binaria</code> resuelve correctamente el ejemplo.
	 * @param a    Arreglo ordenado.
	 * @param num  Número que se busca.
	 * @param v    Respuesta esperada.
	 */
	private static void prueba(int[] a, int num, int v) {
		System.out.println("Arreglo: " + Arrays.toString(a));
		System.out.println("Buscando: " + num);
		
		int i = binaria(a, num);
		System.out.println("índice: " + i);
		System.out.println("correcto: " + v);
		System.out.println(i == v ? "++ Correcto\n" : "-- Fallo\n");
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Pruebas
		int[] a = {1,2,4,6,12,14,24};
		
		// Antes
		prueba(a, 0, -1);
		
		// Después
		prueba(a, 50, -1);
		
		// No está en medio
		prueba(a, 11, -1);
		
		// En medio
		prueba(a, 12, 4);
		
		// Primero
		prueba(a, 1, 0);
		
		// Último
		prueba(a, 24, a.length -1);
		
		System.out.println("Fin");
	}

}
