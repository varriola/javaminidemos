/**
 * 
 */
package minidemos.arreglos;

/**
 * @author blackzafiro
 *
 */
public class Pirámides {
	
	public static int[][] grizz(int n) {
		int[][] r = new int[n][];
		for(int i = n-1; i >= 0; i--) {
			r[i] = new int[n-i];
			for(int j = 0; j < n-i; j++) {
				r[i][j] = (j+1) + (n-i);
			}
		}
		return r;
	}
	
	public static void print(String title, int[][] a) {
		System.out.println(title + ":");
		for(int i = 0; i < a.length; i++) {
			for(int j = 0; j < a[i].length; j++)
				System.out.print("" + a[i][j] + " ");
			System.out.println();
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[][] grizz = grizz(5);
		print("Grizz", grizz);

	}

}
