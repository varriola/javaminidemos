/*
 * Código utilizado para el curso de Introducción a las Ciencias de la
 * Computación.
 * Se permite consultarlo para fines didácticos en forma personal.
 * TODO: Definir comportamiento como en
 * http://learnxinyminutes.com/docs/julia/
 */
package icc.tda.numeros;

/**
 * Cualquier número.
 * @author blackzafiro
 */
public abstract class Numero {
    
    /**
     * Suma dos números realizando las adaptaciones necesarias entre tipos.
     * El tipo del número devuelto debe ser el mismo del número que llama
     * al método (i.e. <code>this</code>).
     * @param n número a sumar
     * @return un nuevo número con el valor del resultado.
     */
    public abstract Numero suma(Numero n);
    public abstract Numero resta(Numero n);
    public abstract Numero multiplica(Numero n);
    public abstract Numero divide(Numero n) throws ArithmeticException;
    
    /**
     * Devuelve una cadena con los ceros y unos de la representación binaria
     * del tipo primitivo donde se encuentra almacenado el valor de este número.
     * @return una cadena con la representación binaria.
     */
    public abstract String representacionBinaria();
    
}
