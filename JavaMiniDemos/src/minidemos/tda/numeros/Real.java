/*
 * Código utilizado para el curso de Introducción a las Ciencias de la
 * Computación.
 * Se permite consultarlo para fines didácticos en forma personal.
 */
package icc.tda.numeros;

/**
 * Tipo numérico que representa números reales.
 * @author blackzafiro
 */
public abstract class Real extends Numero implements Comparable<Real> {
    public abstract Real suma(Numero n);
    public abstract Real resta(Numero n);
    public abstract Real multiplica(Numero n);
    public abstract Real divide(Numero n) throws ArithmeticException;
}
