/*
 * Código utilizado para el curso de Introducción a las Ciencias de la
 * Computación.
 * Se permite consultarlo para fines didácticos en forma personal.
 */
package icc.tda.numeros;

/**
 * Números con punto flotante, almacenados como <code>double</code>.
 * @author blackzafiro
 */
public class Doble extends Real {
    
    /** Número de bits utilizados para almacenar un número en punto flotante. */
    private static final int NUMBITS = 64;
    
    /** Variable primitiva utilizada para almacenar al doble. */
    private double numero;
    
    /** Constructor. */
    public Doble(double n) {
        this.numero = n;
    }
    
    /** Devuelve el valor de este número como un tipo de dato primitivo.
     * 
     * @return una copia del <code>double</code> donde fue almacenado este
     * número.
     */
    public double daPrimitivo() {
        return numero;
    }

    @Override
    public Doble suma(Numero n) {
        if(n instanceof Entero) {
            return suma((Entero)n);
        } else if(n instanceof Doble) {
            return suma((Doble)n);
        }
        throw new ConversionNoSoportadaException("Número " + n);
    }
    
    public Doble suma(Entero n) {
        return new Doble(numero + n.daPrimitivo());
    }
    
    public Doble suma(Doble n) {
        return new Doble(numero + n.numero);
    }

    @Override
    public Doble resta(Numero n) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Doble multiplica(Numero n) {
        if(n instanceof Doble) {
            return multiplica((Doble)n);
        } else if(n instanceof Entero) {
            return multiplica((Entero)n);
        }
        throw new ConversionNoSoportadaException("Número " + n);
    }
    
    public Doble multiplica(Doble n) {
        return new Doble(numero * n.numero);
    }
    
    public Doble multiplica(Entero n) {
        return new Doble(numero * n.daPrimitivo());
    }

    @Override
    public Doble divide(Numero n) throws ArithmeticException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String representacionBinaria() {
        long numero = Double.doubleToRawLongBits(this.numero);
        return Long.toBinaryString(numero);
    }
    
    /**
     * Representación en cadena de este número.
     * @return representación en cadena de este número.
     */
    public String toString() {
        return String.format("%.2f", numero);
    }

    @Override
    public int compareTo(Real o) {
        if(o instanceof Doble) {
            double temp = numero - ((Doble)o).numero;
            return temp < 0 ? -1 : temp == 0 ? 0 : 1;
        }
        if(o instanceof Entero) {
            double temp = numero - ((Entero)o).daPrimitivo();
            return temp < 0 ? -1 : temp == 0 ? 0 : 1;
        }
        throw new ConversionNoSoportadaException("Real " + o);
    }
}
