# JavaMiniDemos

Pequeñas demostraciones de conceptos de Java

## Números

`icc.tda.numeros`

Se ilustra el uso de tipos de datos abstractos mediante la implementación de un sistema para operar con números complejos, reales, dobles y enteros.
Se explota el polimorfismo de Java al utilizar varias formas de sobreescribir y sobrecargar métodos, para que un mismo mensaje tenga respuestas diferentes dependiendo de los tipos de números involucrados. 
Se incluye el uso de `System.in` para recibir entradas del usuario y `Scanner` para analizar cadenas de texto. 

Como muestra, el archivo `UsoNumeros.java` proporciona una calculadora que funciona con una interfaz de usuario de texto.
El patrón *fábrica* aparece para que la interfaz de usuario no necesite conocer los detalles sobre la operación con cada tipo de números, sino que pueda limitarse a lo definido por la clase abstracta `Numero`.
